package controller;

import file_manager.Giocatore;
import file_manager.UserDataManager;
import gui.MyFrame;
import gui.UserChoicePanel;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * 
 * Controller del pannello di scelta utente.
 * 
 * @author Martino De Simoni
 */

/*
 * La classe implementa il pattern mvc e compone il controller.
 * 
 * La classe � difficilmente riutilizzabile. Sarebbe pi� semplice ricominciare
 * da capo e prendere a modello gli altri controller.
 */

public class UserChoiceController extends
		AbstractInsertionPanelController<String, UserChoicePanel> {

	private final UserDataManager dataManager;
	// Stringhe di notifica del controller
	private final String remove = "remove";
	private final String addByDialog = "addByDialog";

	public UserChoiceController(final UserDataManager _dataManager,
			final MyFrame frame, final String terminationString,
			final AbstractMasterPanelController master) {

		this.master = master;
		this.panelID = terminationString;
		this.frame = frame;
		dataManager = _dataManager;

		// Trasformare un HashSet<Giocatore> in un HashSet<String> con i nomi
		// dei giocatori
		this.set = new HashSet<>();
		final Set<Giocatore> giocatori = dataManager.leggiDatiGiocatori();

		for (Giocatore g : giocatori) {
			this.set.add(Utility.tokenToName(g.nome));
		}
		// Fine stralcio

		controlledPanel = new UserChoicePanel(gui.Utility.SFONDO, this.set,
				this, panelID, remove, addByDialog, frame.getSize()); // Il
																		// pannello
																		// deve
																		// stare
																		// qui,
																		// altrimenti:
																		// o
																		// l'oggetto
																		// di
																		// questa
																		// classe
																		// viene
																		// mandato
																		// come
																		// thread,
																		// o
																		// viene
																		// chiesto
																		// l'inserimento
																		// da
																		// JDialog
																		// di un
																		// nome
																		// utente
																		// prima
																		// che
																		// il
																		// pannello
																		// faccia
																		// parte
																		// del
																		// frame
																		// associato.

	}

	/*
	 * L'overload mi serve per programmare la GUI: il MultipleChoicePanel non sa
	 * neanche da dove bisogna togliere la stringa. Mi sembra pi� corretto
	 * nell'ottica di incapsulamento e MVC, ma non � affatto un must. Pi� una
	 * finezza.
	 */

	/**
	 * 
	 * playerNames, in delete e insert, � l'insieme di stringhe che la gui deve
	 * visualizzare, non le etichette dei giocatori.
	 * 
	 * 
	 */

	/**
	 * @param playerName
	 *            Nome del giocatore in forma di stringa
	 */

	public void delete(final String playerName) {

		super.delete(playerName);
		dataManager.cancellaDatiGiocatore(playerName);

	}

	/**
	 * @param playerName
	 *            Nome del giocatore in forma di stringa
	 */

	public void insert(final String playerName) {

		super.insert(Utility.tokenToName(playerName));
		dataManager.appendiDatiGiocatore(new Giocatore(playerName));

	}

	@Override
	public void slaveHasTerminated() {

		this.controlledPanel.setVisible(false);// questo pannello resta in
												// memoria

	}

	/*
	 * Questo metodo non va usato dalla view.
	 * 
	 * Vedo la view come un insieme "stupido" di elementi grafici. Trovo perci�
	 * inopportuno lasciar passare da view degli elementi utili
	 * all'elaborazione, ma farli prelevare dal controller sull'oggetto
	 * controllato.
	 */
	/**
	 * Notifica al controller quale azione intraprendere sull'oggetto
	 * controllato.
	 * 
	 * @param msg
	 *            ID del pulsante passato dal controller al pannello *
	 * 
	 */

	@Override
	public void notifyController(final String msg) {

		if (msg == this.addByDialog) {
			// Per migliorare a facilit� di scrittura su file, non sempre viene
			// stampato quello che viene inserito.
			// Per esempio: Mario_rossi viene salvato come Mario rossi. L'input
			// viene ripulito dei suoi aspetti da token e reso un
			// nome come si � preferito stampare durante la progettazione.
			// TODO Andrebbe un'interfaccia a parte con il metodo inputByDialog,
			// o comunque un metodo di input, per la riutilizzabilit�.
			final String newPlayer = Utility.tokenToName(controlledPanel
					.inputByDialog(gui.Utility.TITLE,
							gui.Utility.ADD_USER_QUESTION));

			if (newPlayer != null) { // Se � stato inserito qualcosa
				if (this.controlledPanel.getChoices().contains(newPlayer)) { // e
																				// quel
																				// qualcosa
																				// �
																				// gi�
																				// stato
																				// inserito
																				// in
																				// precedenza
					this.controlledPanel
							.messageByDialog(gui.Utility.USER_ALREADY_EXISTS_ERROR); // manda
																						// un
																						// messaggio
																						// di
																						// errore
				} else {
					insert(newPlayer);
				} // Altrimenti inseriscilo.
			}

			this.controlledPanel.update(controlledPanel.getGraphics());// Aggiorna
																		// la
																		// GUI.

		}

		else if (msg == this.remove) {

			delete(this.controlledPanel.getList().getSelectedValue());
			this.controlledPanel.update(controlledPanel.getGraphics());

		}

		else if (msg == this.panelID) {

			if (!controlledPanel.getList().isSelectionEmpty()) { // se qualcosa
																	// �
																	// selezionato
				// primo argomento: identificativo. Secondo argomento: giocatore
				// selezionato.
				master.notifyMaster(panelID, dataManager
						.cercaGiocatoreInFile(controlledPanel.getList()
								.getSelectedValue()));
			}

		}
	}

	/*
	 * Lascio notare, che il set di Giocatore � vuoto solo in alcuni casi
	 * all'avvio. Altrimenti, per essere andati oltre il pannello di inserimento
	 * utente, si deve aver creato un utente, quindi si ritorna senza
	 * l'inputDialog.
	 * 
	 * Questo risolve l'eventuale problema di una JDialog di input che compaia
	 * prima che il FrameController abbia selezionato il controlledPanel come
	 * mainPanel del frame associato. In alternativa, per dare sicurezza al
	 * codice, si potrebbe mandare in esecuzione l'UserChoiceController come
	 * Thread, che � facile e veloce. Non mi sento di raccomandare l'utilizzo di
	 * thread per questo gioco, che anche nella versione originale viene
	 * criticato per l'utilizzo grossolano delle prestazioni.
	 */

	public void run() {

		this.controlledPanel.setVisible(true);

		if (this.set.isEmpty())
			this.notifyController(this.addByDialog);

	}

}
