package file_manager;

import java.util.HashMap;
import java.util.Map;

/*
 * 
 * Da interfaccia di gioco, sarebbe davvero possibile aggiungere un editor di livelli. Lascio l'eventualit� del levelWriter 
 * in un package gameDesignToolKit a parte.
 * 
 */

/**
 * 
 * Lettore del file dei livelli.
 * 
 * @author Martino De Simoni
 */

/*
 * Definisco qui la grammatica in BNF.
 * Premetto che nel file possono non esserci livelli, o livelli senza zombie, per semplicit� di programmazione. Confido nel 
 * gameDesigner.
 *
 *_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
 *_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
 *  
 * Sia T il token iniziale
 * 
 * Siano " " e "\n" blank.
 * 
 * Siano token terminali nomeLivello,tempo_in_ms, nome_zombie, lane, soldi, e le stringhe fra doppi apici (").
 * 
 * Sia nihil la stringa vuota.
 * 
 * T:= altriLivelli;
 * altriLivelli:= Livello "\n" altriLivelli | nihil;
 *
 * Livello:= "Inizio_Livello\n" nome altriCampi "Fine_Livello" altriLivelli ;
 * 
 * nome:= "Livello: " nomeLivello "\n";
 * 
 * altriCampi:= "Elenco_zombie\n" altriZombie "Fine_elenco" altriCampi | Ricompensa altriCampi | nihil;
 * 
 * altriZombie := tempo_in_ms nome_zombie lane "\n" altriZombie | nihil;
 * 
 * Ricompensa:= "Soldi: " soldi "\n" | nihil; //Attenzione, soldi � un terminale. Se non c'� nessuna ricompensa, scrivere 0 nel file o come indicato indicato nel metodo di lettura del LevelReader.
 * 
 *_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
 *_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________
 * 
 * 
 * Si apprezzi come l'ordine degli elementi non sia importante.
 * 
 * Sono considerate legali sequenze bislacche con campi ripetuti o senza campi. In effetti, nel caso di aggiunte successive,
 * i vecchi dati utenti mancheranno di campi. Questo non � necessamario, ma confidando nel Writer non si avr� motivo di lanciare 
 * Exceptions.
 * 
 * Si prega di aggiornare la BNF ogniqualvolta necessario.
 * 
 *
 */

/*
 * Sconsiglio fortemente di dare importanza all'ordine dei campi, sia per inutilit�, sia per il bisogno di interpretare una 
 * grammatica di Chomsky contestuale, sia per indebolimento del codice. 
 */

public class LevelReaderImpl extends AbstractFileManager implements LevelReader{

	protected final static String INIZIO_LIVELLO = "Inizio_Livello"; //Ricordo che se il pattern un token->una parola � pi� facile da programmare
	protected final static String FINE_LIVELLO = "Fine_Livello";
	protected final static String NOME_LIVELLO = "Livello:";
	protected final static String RICOMPENSA_IN_DENARO = "Soldi:";
	protected final static String INIZIO_ELENCO_ZOMBIE = "Elenco_Zombie";
	protected final static String FINE_ELENCO_ZOMBIE = "Fine_Elenco";
	
	/**
	 * 
	 * @param filePath Percorso del file
	 */
	public LevelReaderImpl(final String filePath){
	
		super(filePath);

	}
	
	/**
	 * @return La descrizione di ogni livello, presa dal file associata al LevelReader.
	 */
	
	public Map<String,Livello> leggiDatiLivelli(){
		
		final String[] dati = fileToStringArray();

		int contatore=0;
		final Map<String, Livello> livelli = new HashMap<>();
		      
		while(contatore<dati.length-1){ //Lettura di tutti i livelli fino alla fine del file.
		    	  
		    	  final Livello l = new Livello();

		    	  while(!dati[contatore].matches(FINE_LIVELLO)){ //Lettura di un singolo livello
		    		  
		    		  switch(dati[contatore]){
		    		  	case NOME_LIVELLO:
		    		  		l.nome=dati[contatore+1]; contatore++;  break;
		    		  	
		    		  	case INIZIO_ELENCO_ZOMBIE: 
		    		  		contatore++; //cominciano gli zombie. Forma aspettata: tempo_in_ms nomeZombie lane
		    		  		while(!dati[contatore].matches(FINE_ELENCO_ZOMBIE) ) { 
		    		  			l.zombie.add(new EntrataZombie(Integer.parseInt(dati[contatore])
		    		  					,dati[contatore+1],
		    		  					Integer.parseInt(dati[contatore+2]) ) );
		    		  			contatore= contatore +3;
		    		  		} 
		    		  		contatore++; //il while finisce quando dati[contatore]==fineElenco, si passa alla prossima parola
		    		  		break;
		    		  	
		    		  	case RICOMPENSA_IN_DENARO:
		    		  	
		    		  		l.ricompensaInDenaro=Integer.valueOf((Integer.parseInt(dati[contatore+1]))); contatore++; break;
		    		  		 
		    		  		
		    		  	case INIZIO_LIVELLO:
		    		  		contatore++;break; //Non mi viene in mente nessun utilizzo, fineUtente basta e avanza, addirittura si potrebbe lasciare semplicemente il nome come parola di blank fra un utente e l'altro. Comunque, ho visto questo genere di grammatiche spesso e seguo la regola.
		    		  
		    		  	default: {  contatore++; break;} // Per le parole "imprevedibili" (livello_bonus, ZombieConGiornale..)
		    		  }
		    		
		    	  }
		    	  contatore++; // sono su fineUtente, passo avanti
		    	  l.numeroZombie = Integer.valueOf( l.zombie.size() );
		    	  l.zombie.sort( (z1,z2)->z1.tempoInMs-z2.tempoInMs);
		    	  livelli.put(l.nome,l);
		      }
		     
		      return livelli;
		    
			}
		
	
		
	}
	
	