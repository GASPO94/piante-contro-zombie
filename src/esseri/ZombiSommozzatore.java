
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


/*
 * Zombi sommozzatore puo stare solo in un terreno in cui ce l'acqua ed è piu forte di uno zombi comune
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiSommozzatore extends Zombi {

private static String img_filePath = "zombi_sommozzatore.jpeg";
	
	public ZombiSommozzatore(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_MEDIA,Utility.ZOMBIE_DANNO_MEDIO, Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.ACQUA);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}



		
}
