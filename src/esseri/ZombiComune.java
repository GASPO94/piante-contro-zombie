package esseri;


import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


/**
 * 
 * @author Martino De Simoni
 *
 */

public class ZombiComune extends Zombi {
	
	private static String img_filePath = "zombi_comune.jpeg";
	
	public ZombiComune(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_BASSA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);
		
	}



	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D posizione) {
			
		if(canAct(tempo_trascorso)){
		
			Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento( new Posizione2D(-1,0), posizione,this, NessunAzione.getInstance() ),posizione);
			
		
			controller.insert( azioneDaImmettere );
		}
	}




	

}

		

