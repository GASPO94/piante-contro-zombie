/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea
 * 
 *         Pallino � un essere . in fai robe viene indicato l'attacco ,
 *         successivamente il pallino sar� creato dalle sparasemi
 *
 */
// Come pianta e zombie, pallino � un essere
public class Pallino extends Essere {

	private static String img_filePath = "pallino.jpe";
	boolean attaccato = false;

	public Pallino() {

		super(img_filePath, TipoEssere.PALLINO, Utility.PIANTA_VITA_BASSA,
				Utility.PIANTA_DANNO_MEDIO, Utility.PIANTA_TEMPO_MEDIO,
				TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(tempo_trascorso)) {

			Attacco azioneDaImmettere = new Attacco(TipoEssere.ZOMBIE,
					new Posizione2D(1, 0), this.danno, new Movimento(
							new Posizione2D(1, 0), posizione, this,
							NessunAzione.getInstance()), posizione);

			controller.insert(azioneDaImmettere);
		}

	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {
		return life <= 0;

	}

	@Override
	public void prendiDanno(double d) {
		// TODO Auto-generated method stub
		
	}

}
