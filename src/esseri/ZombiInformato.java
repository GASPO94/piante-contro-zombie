package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;


/*
 * Lo zombi infomato è uno zombi inc cui inizialmente i danni li prende l'oggetto che porta con se
 * quando la vita dell'oggetto è uguale a 0 lo zombi informato si arrabbia e 
 * si muove molte piu velocente di prima. 
 * 
 * 
 */

/**
 * 
 * @author Marco Martini
 *
 */


public class ZombiInformato extends Zombi {
	
	
	private static String img_filePath = "zombi_informato.jpeg";
	
	private double lifeOggetto = 30.0;
	
 public ZombiInformato(){
		
		super(img_filePath,Utility.ZOMBIE_VITA_BASSA,Utility.ZOMBIE_DANNO_BASSO, Utility.ZOMBIE_TEMPO_BASSO, TipoTerreno.CORTILE);
		
	}

 /*Override della funzione prendi danno perchè lo zombi infomato inizialmente il danno lo prende l'oggetto
  * e solo quando la vita dell'oggetto è uguale a 0 il danno lo prende lo zombie.
 */
	@Override
	public void prendiDanno(double danno) {
		
		if(this.lifeOggetto !=0){
			
		this.lifeOggetto -= danno;
		
		}
		
		super.prendiDanno(danno);
				
	}

	public double getLifeOggetto(){
		
		return this.lifeOggetto;
	}


	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller,Posizione2D posizione) {


		if(this.canAct(tempo_trascorso)){
			
		if(this.lifeOggetto !=0){
        Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento(new Posizione2D(-1,0),posizione,this, NessunAzione.getInstance() ),posizione);
		
		
		
		controller.insert(azioneDaImmettere);
		}
		
		if(this.lifeOggetto == 0){
		  Attacco azioneDaImmettere = new Attacco( TipoEssere.PIANTA, new Posizione2D(-1,0),this.danno,new Movimento(new Posizione2D(-2,0),posizione,this, NessunAzione.getInstance() ),posizione);
			
			
			
			controller.insert(azioneDaImmettere);
		  }
		}		
	}

	

}
