package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea Noce � una pianta che estende Difensivo, sostanzialmente non
 *         fa nulla , perch� una volta giocata sul terreno deve difendere
 *         dall'attaco degli zombie, quindi la vita di essa � elevata
 *
 *
 */
public class Noce extends Difensivo {

	private static String img_filePath = "noce.jpe";

	public Noce() {

		super(img_filePath, Utility.PIANTA_VITA_ALTA, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D pos) {
	}

	@Override
	public int getSoliRichiesti() {
		return 50;
	}

	@Override
	public TipoTerreno getTerreno() {
		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {
	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}
}